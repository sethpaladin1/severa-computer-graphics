#include <iostream>
#include <string>



class Vector3 {
public:
    float x;
    float y;
    float z;

    // Constructor
    Vector3(float xx, float yy, float zz) : x(xx), y(yy), z(zz) {
        // nothing to do here as we've already initialized x, y, and z above
        std::cout << "Vector3 constructor" << std::endl;
    }

    Vector3() : x(0), y(0), z(0) {
        std::cout << "Vector3 default constructor" << std::endl;
    }

    // Destructor - called when an object goes out of scope or is destroyed
    ~Vector3() {
        // this is where you would release resources such as memory or file descriptors
        // in this case we don't need to do anything
        std::cout << "Vector3 destructor" << std::endl;
    }


};

Vector3 add(const Vector3& v, const Vector3& v2) { // v and v2 are copies, so any changes to them in this function
    // won't affect the originals
    int sumx = v.x + v2.x;
    int sumy = v.y + v2.y;
    int sumz = v.z + v2.z;
    Vector3 r(sumx,sumy,sumz);
    return r;
}

std::ostream& operator<<(std::ostream& stream, const Vector3& v) {
    // std::cout is a std::ostream object, just like stream
    // so use stream as if it were cout and output the components of
    // the vector

    stream << v.x << ", " << v.y << ", " << v.z;

    return stream;
}

Vector3 operator+(const Vector3& v, const Vector3& v2){
    return add(v, v2);
}

int main(int argc, char** argv) {

    std::cout << argc << "," << argv[0];
    std::string name =  "";
    std::cin >> name;
    std::cout << "hello <" << name << ">" << std::endl;


    Vector3 a(1,2,3);   // allocated to the stack
    Vector3 b(4,5,6);

    std::cout << a + b << std::endl;

    Vector3* v3 =  new Vector3(0,0,0);
    v3->y = 5;

    std::cout << v3 << std::endl;
    delete v3;


    Vector3 array[10];

    Vector3* array2 = new Vector3[10];

    for (int i = 0; i < 10; i++)
    {
        array2[i].y = 5;
    }

    for (int i = 0; i < 10; i++)
    {
        std::cout << array2[i] << std::endl;
    }

    delete [] array2;

}